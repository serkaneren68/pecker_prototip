################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.c \
../Core/Tracealyzer/extras/ExtensionExample/source/main.c 

OBJS += \
./Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.o \
./Core/Tracealyzer/extras/ExtensionExample/source/main.o 

C_DEPS += \
./Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.d \
./Core/Tracealyzer/extras/ExtensionExample/source/main.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/extras/ExtensionExample/source/%.o Core/Tracealyzer/extras/ExtensionExample/source/%.su: ../Core/Tracealyzer/extras/ExtensionExample/source/%.c Core/Tracealyzer/extras/ExtensionExample/source/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx '-DCMSIS_device_header=<stm32g0xx.h>' -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-extras-2f-ExtensionExample-2f-source

clean-Core-2f-Tracealyzer-2f-extras-2f-ExtensionExample-2f-source:
	-$(RM) ./Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.d ./Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.o ./Core/Tracealyzer/extras/ExtensionExample/source/MyExtension.su ./Core/Tracealyzer/extras/ExtensionExample/source/main.d ./Core/Tracealyzer/extras/ExtensionExample/source/main.o ./Core/Tracealyzer/extras/ExtensionExample/source/main.su

.PHONY: clean-Core-2f-Tracealyzer-2f-extras-2f-ExtensionExample-2f-source

