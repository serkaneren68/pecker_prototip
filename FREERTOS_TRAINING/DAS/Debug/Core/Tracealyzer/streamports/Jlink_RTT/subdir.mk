################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.c \
../Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.c 

OBJS += \
./Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.o \
./Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.o 

C_DEPS += \
./Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.d \
./Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/streamports/Jlink_RTT/%.o Core/Tracealyzer/streamports/Jlink_RTT/%.su: ../Core/Tracealyzer/streamports/Jlink_RTT/%.c Core/Tracealyzer/streamports/Jlink_RTT/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx '-DCMSIS_device_header=<stm32g0xx.h>' -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-streamports-2f-Jlink_RTT

clean-Core-2f-Tracealyzer-2f-streamports-2f-Jlink_RTT:
	-$(RM) ./Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.d ./Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.o ./Core/Tracealyzer/streamports/Jlink_RTT/SEGGER_RTT.su ./Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.d ./Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.o ./Core/Tracealyzer/streamports/Jlink_RTT/trcStreamPort.su

.PHONY: clean-Core-2f-Tracealyzer-2f-streamports-2f-Jlink_RTT

