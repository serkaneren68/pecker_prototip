################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/streamports/TCPIP/trcStreamPort.c 

OBJS += \
./Core/Tracealyzer/streamports/TCPIP/trcStreamPort.o 

C_DEPS += \
./Core/Tracealyzer/streamports/TCPIP/trcStreamPort.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/streamports/TCPIP/%.o Core/Tracealyzer/streamports/TCPIP/%.su: ../Core/Tracealyzer/streamports/TCPIP/%.c Core/Tracealyzer/streamports/TCPIP/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G030xx '-DCMSIS_device_header=<stm32g0xx.h>' -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-streamports-2f-TCPIP

clean-Core-2f-Tracealyzer-2f-streamports-2f-TCPIP:
	-$(RM) ./Core/Tracealyzer/streamports/TCPIP/trcStreamPort.d ./Core/Tracealyzer/streamports/TCPIP/trcStreamPort.o ./Core/Tracealyzer/streamports/TCPIP/trcStreamPort.su

.PHONY: clean-Core-2f-Tracealyzer-2f-streamports-2f-TCPIP

