################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/extras/RunnableExample/source/main.c 

OBJS += \
./Core/Tracealyzer/extras/RunnableExample/source/main.o 

C_DEPS += \
./Core/Tracealyzer/extras/RunnableExample/source/main.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/extras/RunnableExample/source/%.o Core/Tracealyzer/extras/RunnableExample/source/%.su: ../Core/Tracealyzer/extras/RunnableExample/source/%.c Core/Tracealyzer/extras/RunnableExample/source/subdir.mk
	arm-none-eabi-gcc "$<" -std=gnu11 -g -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-extras-2f-RunnableExample-2f-source

clean-Core-2f-Tracealyzer-2f-extras-2f-RunnableExample-2f-source:
	-$(RM) ./Core/Tracealyzer/extras/RunnableExample/source/main.d ./Core/Tracealyzer/extras/RunnableExample/source/main.o ./Core/Tracealyzer/extras/RunnableExample/source/main.su

.PHONY: clean-Core-2f-Tracealyzer-2f-extras-2f-RunnableExample-2f-source

