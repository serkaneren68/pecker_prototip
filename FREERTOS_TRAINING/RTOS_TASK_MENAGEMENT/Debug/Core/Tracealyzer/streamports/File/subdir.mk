################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/streamports/File/trcStreamPort.c 

OBJS += \
./Core/Tracealyzer/streamports/File/trcStreamPort.o 

C_DEPS += \
./Core/Tracealyzer/streamports/File/trcStreamPort.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/streamports/File/%.o Core/Tracealyzer/streamports/File/%.su: ../Core/Tracealyzer/streamports/File/%.c Core/Tracealyzer/streamports/File/subdir.mk
	arm-none-eabi-gcc "$<" -std=gnu11 -g -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-streamports-2f-File

clean-Core-2f-Tracealyzer-2f-streamports-2f-File:
	-$(RM) ./Core/Tracealyzer/streamports/File/trcStreamPort.d ./Core/Tracealyzer/streamports/File/trcStreamPort.o ./Core/Tracealyzer/streamports/File/trcStreamPort.su

.PHONY: clean-Core-2f-Tracealyzer-2f-streamports-2f-File

