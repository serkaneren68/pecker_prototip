################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.c 

OBJS += \
./Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.o 

C_DEPS += \
./Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Tracealyzer/streamports/RingBuffer/%.o Core/Tracealyzer/streamports/RingBuffer/%.su: ../Core/Tracealyzer/streamports/RingBuffer/%.c Core/Tracealyzer/streamports/RingBuffer/subdir.mk
	arm-none-eabi-gcc "$<" -std=gnu11 -g -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Tracealyzer-2f-streamports-2f-RingBuffer

clean-Core-2f-Tracealyzer-2f-streamports-2f-RingBuffer:
	-$(RM) ./Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.d ./Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.o ./Core/Tracealyzer/streamports/RingBuffer/trcStreamPort.su

.PHONY: clean-Core-2f-Tracealyzer-2f-streamports-2f-RingBuffer

