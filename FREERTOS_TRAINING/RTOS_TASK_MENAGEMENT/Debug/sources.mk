################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
SU_FILES := 
EXECUTABLES := 
OBJS := 
MAP_FILES := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Core/Src \
Core/Startup \
Core/Tracealyzer/extras/ExtensionExample/source \
Core/Tracealyzer/extras/RunnableExample/source \
Core/Tracealyzer/streamports/ARM_ITM \
Core/Tracealyzer/streamports/File \
Core/Tracealyzer/streamports/Jlink_RTT \
Core/Tracealyzer/streamports/RingBuffer \
Core/Tracealyzer/streamports/STM32_USB_CDC \
Core/Tracealyzer/streamports/TCPIP \
Core/Tracealyzer/streamports/TCPIP_Win32 \
Core/Tracealyzer/streamports/UDP \
Core/Tracealyzer/streamports/XMOS_xScope \
Core/Tracealyzer \
Drivers/STM32G0xx_HAL_Driver/Src \
Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 \
Middlewares/Third_Party/FreeRTOS/Source \
Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0 \
Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang \

